package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {

	public CreateLeadPage() {
		
		PageFactory.initElements(driver, this);
		}
	
	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement eleCompname;
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how = How.XPATH,using="//input[@value = 'Create Lead") WebElement eleCLbutton;
	
	public CreateLeadPage enterCompanyName(String data ) {
		clearAndType(eleCompname, data);
		return this;
	}

	public CreateLeadPage enterFirstName(String data) {
		clearAndType(eleFirstName, data);
		return this;
	}
	public CreateLeadPage enterLastName(String data) {
		clearAndType(eleLastName, data);
		return this;
	}
	public ViewPage clickSubmit() {
		click(eleCLbutton);
		return new ViewPage();
	}
}

