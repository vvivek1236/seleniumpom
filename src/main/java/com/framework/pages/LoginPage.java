package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;

public class LoginPage extends ProjectMethods{

	
	public LoginPage() {
		//apply PageFactory
		//PageFactory created under Constructor since it is the entry point for all the classes
		PageFactory.initElements(driver, this); 
	}
	// FindBy Annotation would be always at class level
	@FindBy(how = How.ID,using="username") WebElement eleUsername;
	@FindBy(how = How.ID,using="password") WebElement elePassword;
	@FindBy(how = How.CLASS_NAME,using="decorativeSubmit") WebElement eleLogin;
	
	@Test
	public LoginPage enterUsername(String data) {
		//WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, data);
		return this; 
	}
	public LoginPage enterPassword(String data) {
		//WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, data);
		return this;
	}
	public HomePage clickLogin() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
	    click(eleLogin);
	    /*HomePage hp = new HomePage();
	    return hp;*/ 
	    return new HomePage();
	}
}













